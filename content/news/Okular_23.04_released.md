---
date: 2023-04-20
title: Okular 23.04 released
---
The 23.04 version of Okular has been released. This release contains various minor fixes and feature enhancements. You can check the full changelog at https://kde.org/announcements/changelogs/gear/23.04.0/#okular. Okular 23.04 is a recommended update for everyone using Okular.
