---
date: 2022-12-08
title: Okular 22.12 released
---
The 22.12 version of Okular has been released. This release contains various minor fixes and feature enhancements. You can check the full changelog at https://kde.org/announcements/changelogs/gear/22.12.0/#okular. Okular 22.12 is a recommended update for everyone using Okular.
