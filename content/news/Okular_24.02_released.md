---
date: 2024-02-28
title: Okular 24.02 released
categories:
 - Releases
---
The 24.02 version of Okular has been released. You can check the full changelog at https://kde.org/announcements/changelogs/gear/24.02.0/#okular. Okular 24.02 is a recommended update for everyone using Okular.
