---
date: 2022-04-21
title: Okular 22.04 released
---
The 22.04 version of Okular has been released. The main changes of this release are the new Welcome Screen and fixes related to signing specially crafted PDF files, but it also contains various minor fixes and features. You can check the full changelog at https://kde.org/announcements/changelogs/gear/22.04.0/#okular. Okular 22.04 is a recommended update for everyone using Okular.
