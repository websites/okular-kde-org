---
date: 2022-08-18
title: Okular 22.08 released
---
The 22.08 version of Okular has been released. This release contains various minor fixes and feature enhacements. You can check the full changelog at https://kde.org/announcements/changelogs/gear/22.08.0/#okular. Okular 22.08 is a recommended update for everyone using Okular.
