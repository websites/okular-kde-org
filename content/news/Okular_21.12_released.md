---
date: 2021-12-09
title: Okular 21.12 released
---
The 21.12 version of Okular has been released. The main improvement of this release is proper support of Stamps in PDF files, but it also contains various minor fixes and features. You can check the full changelog at <a href='https://kde.org/announcements/changelogs/gear/21.12.0/#okular'>https://kde.org/announcements/changelogs/gear/21.12.0/#okular</a>. Okular 21.12 is a recommended update for everyone using Okular.
