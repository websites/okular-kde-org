---
date: 2019-12-12
title: Okular 1.9 released
---
The 1.9 version of Okular has been released. This release introduces cb7 support for ComicBook archives, improved JavaScript support for PDF files among various other fixes and small features. You can check the full changelog at <a href='https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=19.12.0#okular</a>. Okular 1.9 is a recommended update for everyone using Okular.