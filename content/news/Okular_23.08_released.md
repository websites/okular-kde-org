---
date: 2023-08-24
title: Okular 23.08 released
categories:
 - Releases
---
The 23.08 version of Okular has been released. This release contains various minor fixes and feature enhancements. You can check the full changelog at https://kde.org/announcements/changelogs/gear/23.08.0/#okular. Okular 23.08 is a recommended update for everyone using Okular.
