---
date: 2024-12-12
title: Okular 24.12 released
categories:
 - Releases
---
The 24.12 version of Okular has been released. This release includes several improvements in PDF form handling and Signature Creation as well as various other minor fixes and feature enhancements. You can check the full changelog at https://kde.org/announcements/changelogs/gear/24.12.0/#okular.
