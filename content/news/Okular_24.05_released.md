---
date: 2024-05-23
title: Okular 24.05 released
categories:
 - Releases
---
The 24.05 version of Okular has been released. You can check the full changelog at https://kde.org/announcements/changelogs/gear/24.05.0/#okular. Okular 24.05 is a recommended update for everyone using Okular.
