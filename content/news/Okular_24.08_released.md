---
date: 2024-08-22
title: Okular 24.08 released
categories:
 - Releases
---
The 24.08 version of Okular has been released. This release includes several improvements in PDF form handling and various other minor fixes and feature enhancements. You can check the full changelog at https://kde.org/announcements/changelogs/gear/24.08.0/#okular.
