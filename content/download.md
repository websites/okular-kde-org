---
layout: download
title: Download
subtitle: Okular is available as a precompiled package in a wide range of platforms. You can check the package status for your Linux distro on the right or keep reading for info on other operating systems
appstream: org.kde.okular
name: Okular
gear: true
flatpak: true
menu:
  main:
    parent: about
    weight: 3
scssFiles:
- /scss/download.scss
sources:
- name: Windows
  src_icon: /reusable-assets/windows.svg
  description: "The [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8) is the recommended place to install Okular, the version there has been tested by our developers and the Microsoft Store provides seamless updates when new versions are released."
pology: okular
nightly:
 - name: Windows
   link: 'https://cdn.kde.org/ci-builds/graphics/okular/master/windows/'
 - name: macOS (ARM)
   link: 'https://cdn.kde.org/ci-builds/graphics/okular/master/macos-arm64/'
 - name: macOS (Intel)
   link: 'https://cdn.kde.org/ci-builds/graphics/okular/master/macos-x86_64/'
---
