# Okular Website

This is the git repository for [okular.kde.org](https://okular.kde.org), the website for Okular, the universal document reader developed by KDE.

As a (Hu)Go module, it requires both [Hugo](https://gohugo.io/) and Go to work.

## Development
Read [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/).

## I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n).
